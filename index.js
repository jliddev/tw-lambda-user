require('dotenv').config();
const jwt = require('jsonwebtoken');
const { Client } = require('pg');

let _connectionStr = `postgres://${process.env.POSTGRES_USERNAME}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT || '5432'}/${process.env.POSTGRES_DATABASE}`
let _client = null;

function getClient() {
    if (_client !== null) {
        return new Promise((res) => res());
    }
    else {
        _client = new Client(_connectionStr);
        return _client.connect();
    }
}

function shutdown() {
    if (_client !== null) {
        _client.end();
        _client = null;
    }
}
exports.shutdown = shutdown;

exports.getConnectionString = function () {
    return _connectionStr.slice(0);
}

exports.setConnectionString = function (cstr) {
    if (typeof cstr === 'string' && cstr.length > 0) {
        _connectionStr = cstr;
    }
}

exports.getToken = function (params) {
    var token;

    if (params.headers) {
        if (params.headers.Authorization) {
            var tokenString = params.headers.Authorization;
            var match = tokenString.match(/^Bearer (.*)$/);
            if (match && match.length >= 2) {
                return match[1];
            }
        }
    }
    else if (params.type && params.type === 'TOKEN') {
        var tokenString = params.authorizationToken;
        if (tokenString) {
            var match = tokenString.match(/^Bearer (.*)$/);
            if (match && match.length >= 2) {
                return match[1];
            }
        }
    }
    return null;
}

exports.decodeToken = function (token) {
    return jwt.decode(token);
}

exports.executeQuery = function (sql, args) {
    return getClient()
        .then(() => {
            return _client.query(sql, args)
        })
        .then(res => {
            return res.rows;
        })
        .catch(e => {
            console.error(e);
        });

}

exports.hasPermissionsAsync = function (userId, permissionName) {
    return getUserPermissions(userId)
        .then(permissions => {
            for (let i = 0, len = permissions.length; i < len; i += 1) {
                if (permissions[i].name === permissionName) {
                    return true;
                }
            }
            return false;
        });
}

exports.hasPermissions = function (userMetadata, permissionName) {
    if (Array.isArray(userMetadata.permissions)) {
        for (let i = 0, len = userMetadata.permissions.length; i < len; i += 1) {
            if (userMetadata.permissions[i].name === permissionName) {
                return true;
            }
        }
    }
    return false;
}

exports.getUserMetadata = function (userId, options) {
    if (!_connectionStr) {
        return new Promise((res, rej) => rej(new Error('connection string not initialized')));
    }

    let _options = Object.assign({}, options);

    return getClient()
        .then(() => {
            let tasks = [
                getUserPermissions(userId)
            ];

            tasks.push(_options.roles ? getUserRoles(_client, userId) : undefined);

            return Promise.all(tasks)
        })
        .then(res => {
            // console.log('QUERY DONE', res)
            return {
                permissions: res[0],
                roles: res[1]
            }
        })
        .catch(e => {
            console.error(e);
        });
}

function getUserPermissions(userId) {
    return getClient()
        .then(() => _client.query('SELECT * FROM fn_user_permissions( $1 )', [userId]))
        .then(res => res.rows);
}

function getUserRoles(client, userId) {
    return client.query('SELECT * FROM fn_user_roles( $1 )', [userId])
        .then(res => res.rows);
}

process.on('exit', () => {
    shutdown();
});

// exports.getUserMetadata('auth0|59a618324ab1a83f1a97363d', { roles: true })
//     .then(userMeta => {
//         let hasPerm = exports.hasPermissions(userMeta, 'admin:all');
//         console.log('aa');
//         shutdown();
//     });

